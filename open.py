import subprocess
import time
import os

folder = "/Users/anthonybloomer/Downloads/"
application = "mpv"

def get_filepaths(folder):
    file_paths = []
    [file_paths.append(os.path.join(root, filename)) for root, directories, files in os.walk(folder) for filename in files]
    return file_paths

while True:
    d1 = get_filepaths(folder)
    time.sleep(2)
    d2 = get_filepaths(folder)
    for file in [f for f in d2 if f not in d1 and f.endswith('.webm') and 'download' not in f]:
        command = application + ' --volume 0 ' + file
        subprocess.Popen(["/bin/bash", "-c", command])
