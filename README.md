#webm-mpv

Safari is my preferred browser but it doesn't have WebM support. This python script watches my download folder for new WebM files and opens them in mpv automatically.
